<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login','ApiController@login');
Route::post('signup', 'ApiController@signup');
Route::get('roles', 'RoleController@roles');
Route::get('cortes', 'CorteProgramaController@cortes');
Route::get('programas/names', 'ProgramaController@nombresProgramas');
Route::get('programas', 'ProgramaController@index');
Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'ApiController@logout');
        Route::get('user', 'ApiController@user');
		Route::put('users/{id}', 'UserController@update');
		Route::get('users', 'UserController@index');
		Route::get('user/{id}', 'UserController@show');
		Route::delete('programas/{id}', 'ProgramaController@delete');
		Route::delete('users/{id}', 'UserController@delete');
		Route::get('programas/{id}', 'ProgramaController@show');
		Route::post('programas', 'ProgramaController@store');
		Route::put('programas/{id}', 'ProgramaController@update');
		Route::put('notificaciones/{fk_user}', 'NotificacionController@nuevas');
		Route::put('notificaciones/{fk_user}', 'NotificacionController@viejas');
		//Route::get('programas/autorizados/{id}', 'ProgramaController@usuariosAutorizados');
		Route::get('programas/autorizados/{id}', 'UserController@programasAutorizados');
    });
