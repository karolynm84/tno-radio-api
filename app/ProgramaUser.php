<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramaUser extends Model
{
    protected $fillable = [
		 'user_id',
		 'programa_id'
	];
}
