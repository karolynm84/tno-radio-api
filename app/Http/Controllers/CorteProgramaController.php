<?php

namespace App\Http\Controllers;
use App\CortePrograma;

use Illuminate\Http\Request;

class CorteProgramaController extends Controller
{
   public function index()
    {
        return CortePrograma::all();
    }
	
	public function cortes()
    {
        return CortePrograma::select('corte')->orderBy("corte", "asc")->get();
    }
 
    public function show($id)
    {
        return CortePrograma::find($id);
    }

    public function store(Request $request)
    {
        return CortePrograma::create($request->all());
    }

    public function update(Request $request, $id)
    {
        $CortePrograma = CortePrograma::findOrFail($id);
        $CortePrograma->update($request->all());

        return $CortePrograma;
    }

    public function delete(Request $request, $id)
    {
        $CortePrograma = CortePrograma::findOrFail($id);
        $CortePrograma->delete();

        return 204;
    }
}
