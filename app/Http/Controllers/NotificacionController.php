<?php

namespace App\Http\Controllers;
use App\Notificacion;

use Illuminate\Http\Request;

class NotificacionController extends Controller
{
   public function index()
    {
        return Notificacion::all();
    }
 
    public function show($id)
    {
        return Notificacion::find($id);
    }

    public function store(Request $request)
    {
        return Notificacion::create($request->all());
    }

    public function update(Request $request, $id)
    {
        $Notificacion = Notificacion::findOrFail($id);
        $Notificacion->update($request->all());

        return $Notificacion;
    }

    public function delete(Request $request, $id)
    {
        $Notificacion = Notificacion::findOrFail($id);
        $Notificacion->delete();

        return 204;
    }
	
	public function nuevas($fk_user)
    {
		$notificaciones = DB::table('notificacion')->where([
								['leido', false],
								['fk_user', $fk_user],
							])->get();
        return $notificaciones;
    }
	public function viejas()
    {
       $notificaciones = DB::table('notificacion')->where([
								['leido', true],
								['fk_user', $fk_user],
							])->get();
        return $notificaciones;
    }
}
