<?php

namespace App\Http\Controllers;
use App\Programa;
use App\User;

use Illuminate\Http\Request;

class ProgramaController extends Controller
{
   public function index()
    {
        return Programa::all();
    }
 
    public function show($id)
    {
        return Programa::find($id);
    }

    public function store(Request $request)
    {
		$programa = new Programa;
        $programa->pro_nombre = $request->pro_nombre;
        $programa->pro_sinopsis = $request->pro_sinopsis;
        $programa->pro_email = $request->pro_email;
        $programa->pro_hora_inicio = $request->pro_hora_inicio;
        $programa->pro_hora_fin = $request->pro_hora_fin;
        $programa->pro_fecha_origen = $request->pro_fecha_origen;
        $programa->pro_dia_semana = $request->pro_dia_semana;
        $programa->pro_productor = $request->pro_productor;
        $programa->pro_locutor_1 = $request->pro_locutor_1;
        $programa->pro_locutor_2 = $request->pro_locutor_2;
        $programa->pro_locutor_3 = $request->pro_locutor_3;
        $programa->pro_locutor_4 = $request->pro_locutor_4;
        $programa->pro_instagram = $request->pro_instagram;
        $programa->pro_facebook = $request->pro_facebook;
        $programa->pro_snapchat = $request->pro_snapchat;
        $programa->pro_twitter = $request->pro_twitter;
        $programa->fk_user_last_modifier = $request->fk_user_last_modifier;        
        $programa->habilitado = $request->habilitado;        
        $programa->save();

        $user = User::find([$request->fk_user_last_modifier]);
        $programa->users()->attach($user);
        
		return $programa;
    }	
		

    public function update(Request $request, $id)
    {
        $Programa = Programa::findOrFail($id);
        $Programa->update($request->all());

        return $Programa;
    }

    public function delete(Request $request, $id)
    {
        $Programa = Programa::findOrFail($id);
        $Programa->delete();

        return 204;
    }
	
	public function nombresProgramas()
    {
        return Programa::select('pro_nombre')->get();
    }
	
	public function usuariosAutorizados($id) {
		$Programa = Programa::find($id);
		
		return $Programa ->users()->get();
	}
	//}
}
