<?php

namespace App\Http\Controllers;
use App\Role;

use Illuminate\Http\Request;

class RoleController extends Controller
{
   public function index()
    {
        return Role::all();
    }
	
	public function roles()
    {
        return Role::select('role')->get();
    }
 
    public function show($id)
    {
        return Role::find($id);
    }

    public function store(Request $request)
    {
        return Role::create($request->all());
    }

    public function update(Request $request, $id)
    {
        $Role = Role::findOrFail($id);
        $Role->update($request->all());

        return $Role;
    }

    public function delete(Request $request, $id)
    {
        $Role = Role::findOrFail($id);
        $Role->delete();

        return 204;
    }
}
