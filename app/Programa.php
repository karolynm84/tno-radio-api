<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Programa extends Model
{
    protected $fillable = [
				'pro_nombre', 
				'pro_sinopsis', 
				'pro_email', 
				'pro_hora_inicio', 
				'pro_hora_fin', 
				'pro_dia_semana', 
				'pro_fecha_origen',
				'pro_locutor_1',
				'pro_locutor_2',
				'pro_locutor_3',
				'pro_locutor_4',
				'pro_productor',
				'pro_instagram',
				'pro_twitter',
				'pro_snapchat',
				'pro_facebook',
				'habilitado',
				'fk_user_last_modifier',
				'pro_corte'
	];
	
	public function users () {
		return $this->belongsToMany(User::class)->withTimestamps();;
	}
}
