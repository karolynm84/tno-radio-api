<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CortePrograma extends Model
{
    protected $fillable = [
        'corte'
    ];
}
