<?php

use Illuminate\Database\Seeder;

class CorteProgramaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('corte_programas')->insert([
			'corte' => 'Salud',
        ]);
		DB::table('corte_programas')->insert([
			'corte' => 'Religioso',
		]); 
		DB::table('corte_programas')->insert([
			'corte' => 'Motivacional',
		]);
		DB::table('corte_programas')->insert([
			'corte' => 'Holístico',
        ]);
		DB::table('corte_programas')->insert([
			'corte' => 'Cacao',
        ]);
		DB::table('corte_programas')->insert([
			'corte' => 'Talk Show',
        ]);
		DB::table('corte_programas')->insert([
			'corte' => 'Juvenil',
        ]);
		DB::table('corte_programas')->insert([
			'corte' => 'Entrevistas',
        ]);
		DB::table('corte_programas')->insert([
			'corte' => 'Franquicias',
        ]);
		DB::table('corte_programas')->insert([
			'corte' => 'Economía y Negocios',
        ]);
		DB::table('corte_programas')->insert([
			'corte' => 'Estudiantil',
		]); 
		DB::table('corte_programas')->insert([
			'corte' => '7mo. Arte',
		]);
		DB::table('corte_programas')->insert([
			'corte' => 'Promoción Musical',
        ]);
		DB::table('corte_programas')->insert([
			'corte' => 'Movida Caraqueña',
        ]);
		DB::table('corte_programas')->insert([
			'corte' => 'Revista',
        ]);
		DB::table('corte_programas')->insert([
			'corte' => 'Promoción Artística',
        ]);
		DB::table('corte_programas')->insert([
			'corte' => 'Gastronomía y Bebidas',
        ]);
		DB::table('corte_programas')->insert([
			'corte' => 'Emprendimiento',
        ]);
		DB::table('corte_programas')->insert([
			'corte' => 'Fitness',
        ]);
		DB::table('corte_programas')->insert([
			'corte' => 'Mascotas',
		]); 
		DB::table('corte_programas')->insert([
			'corte' => 'Teatro',
		]);
		DB::table('corte_programas')->insert([
			'corte' => 'Espectáculos',
        ]);
		DB::table('corte_programas')->insert([
			'corte' => 'Baloncesto',
        ]);
		DB::table('corte_programas')->insert([
			'corte' => 'Motivación Femenina',
        ]);
		DB::table('corte_programas')->insert([
			'corte' => 'Musical',
        ]);
		DB::table('corte_programas')->insert([
			'corte' => 'Educación Sexual',
        ]);
		DB::table('corte_programas')->insert([
			'corte' => 'Arte',
        ]);
		DB::table('corte_programas')->insert([
			'corte' => 'Fútbol',
        ]);
		DB::table('corte_programas')->insert([
			'corte' => 'Economía Digital',
		]); 
		DB::table('corte_programas')->insert([
			'corte' => 'Fotografía',
		]);
		DB::table('corte_programas')->insert([
			'corte' => 'Noticias Deportivas',
        ]);
    }
}
