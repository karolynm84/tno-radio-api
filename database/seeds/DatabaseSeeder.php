<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$this->call(ProgramaTableSeeder::class);
        $this->call(RoleTableSeeder::class);
		$this->call(UserTableSeeder::class);
		$this->call(SolicitudTarifasTableSeeder::class);		
		$this->call(SolicitudEspacioTableSeeder::class);		
		$this->call(SolicitudPautaTableSeeder::class);		
		$this->call(SolicitudPatrocinioTableSeeder::class);		
		$this->call(NotificacionTableSeeder::class);		
		$this->call(ProgramaUserTableSeeder::class);		
		$this->call(CorteProgramaTableSeederTableSeeder::class);		
    }
}
