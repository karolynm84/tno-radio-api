<?php

use Illuminate\Database\Seeder;

class ProgramaUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('programa_user')->insert([
			'user_id' => 1,
			'programa_id' =>1
        ]);
    }
}
