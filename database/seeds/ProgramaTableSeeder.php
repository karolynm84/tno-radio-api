<?php

use Illuminate\Database\Seeder;

class ProgramaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('programas')->insert([
			'pro_nombre' => 'PROGRAMA DE PRUEBA',
			'pro_sinopsis' => 'Este es el programa de prueba',
			'pro_email' => str_random(10).'@gmail.com',
			'pro_hora_inicio' => '09:00',
			'pro_hora_fin' => '10:00',
			'pro_fecha_origen' => '2014-01-01',
			'pro_dia_semana' => 'miércoles',
			'pro_productor' => 'Carla Rubera',
			'pro_locutor_1' => 'Beba Vendenberg',			
			'pro_locutor_2' => null,			
			'pro_locutor_3' => null,			
			'pro_locutor_4' => null,	
			'pro_instagram' => null,
			'pro_facebook' => null,
			'pro_twitter' => null,
			'pro_snapchat' => null,
			'fk_user_last_modifier' => 1,
			'habilitado'=>true
        ]);
    }
}