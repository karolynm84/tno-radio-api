<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('users')->insert([
			'name' => 'Karolyn',
			'lastname' => 'Méndez',
			'email' => 'karolyn@tnoradio.com',
			'password' => bcrypt('111111'),			
			'role' => 'Administrador',
			'us_id' => 'V16644576',
			'pni' => '',
			'c_locucion' => '',
			'telephone' => '+58 412-3964576',
			'birthdate' => '1984-07-30',
			'instagram' =>'@lamitologica',
			'twitter' => '@lamitologica',
			'facebook' => 'http://facebook.com/lamitologica',
			'bio' => 'Junior DTI',
			'enabled' => true
        ]);   
    }
}
