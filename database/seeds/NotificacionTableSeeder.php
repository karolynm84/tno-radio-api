<?php

use Illuminate\Database\Seeder;

class NotificacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('notificacion')->insert([
			'notificacion' => 'Bienvenido a TNO Radio Sistema.',
			'tipo' => 'felicidad',
			'leido' => false,
			'fk_user'=> 1
        ]);
    }
}
