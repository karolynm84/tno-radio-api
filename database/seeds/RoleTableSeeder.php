<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
			'role' => 'Productor',
        ]);
		DB::table('roles')->insert([
			'role' => 'Locutor',
		]); 
		DB::table('roles')->insert([
			'role' => 'Audiencia',
		]);
		DB::table('roles')->insert([
			'role' => 'Operador',
        ]);
		DB::table('roles')->insert([
			'role' => 'Anunciante',
        ]);
		DB::table('roles')->insert([
			'role' => 'Productor y Locutor',
        ]);
		DB::table('roles')->insert([
			'role' => 'Asistente de Producción',
        ]);
		DB::table('roles')->insert([
			'role' => 'Social Media Manager',
        ]);
		DB::table('roles')->insert([
			'role' => 'Community Manager',
        ]);
    }
}
