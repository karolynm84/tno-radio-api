<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
			$table->string('lastname');
            $table->string('email')->unique();
			$table->string('role');
			$table->foreign('role')->references('role')->on('roles');
            $table->string('password');
			$table->string('pni')->nullable();
			$table->string('us_id')->nullable();
			$table->string('c_locucion')->nullable();
			$table->string('telephone')->nullable();
			$table->date('birthdate')->nullable();
			$table->string('instagram')->nullable();
			$table->string('twitter')->nullable();
			$table->string('facebook')->nullable();
			$table->boolean('enabled');
			$table->string('bio',500)->nullable();			
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
