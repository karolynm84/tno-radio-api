<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programas', function (Blueprint $table) {
            $table->increments('id');
			$table->string('pro_nombre')->default('nombre')->unique();
			$table->string('pro_sinopsis', 500)->default('Descripción corta del programa');
			$table->time('pro_hora_inicio')->default('00:00');
			$table->time('pro_hora_fin')->default('00:00');
			$table->date('pro_fecha_origen')->default('2014-01-01');
			$table->string('pro_email', 50)->unique()->default('email@email.com');
			$table->string('pro_productor')->default('nombre del productor');
			$table->string('pro_dia_semana')->default('{dia_1,dia_2,dia_3}');
			$table->string('pro_corte')->default('Salud');
			$table->string('pro_instagram')->default('instagram')->nullable();
			$table->string('pro_twitter')->default('twitter')->nullable();
			$table->string('pro_facebook')->default('facebook')->nullable();
			$table->string('pro_snapchat')->default('snapchat')->nullable();
			$table->string('pro_locutor_1')->default('Locutor_1');
			$table->string('pro_locutor_2')->default('Locutor_2')->nullable();
			$table->string('pro_locutor_3')->default('Locutor_3')->nullable();
			$table->string('pro_locutor_4')->default('Locutor_4')->nullable();
			$table->string('habilitado')->default('boolean')->nullable();	
			$table->unsignedInteger('fk_user_last_modifier');
			$table->foreign('fk_user_last_modifier')->references('id')->on('users')->nullable()->onDelete('cascade');			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programas');
    }
}
